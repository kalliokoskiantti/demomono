package fi.antti.sensitivewriter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SensitivewriterApplication {

    public static void main(String[] args) {
        SpringApplication.run(SensitivewriterApplication.class, args);
    }

}
