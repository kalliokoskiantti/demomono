package fi.antti.restservice.service;

import fi.antti.restservice.domain.DataEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@RestController
public class PatientService {

    private PatientRepository patientRepository;

    @Autowired
    KafkaTemplate kafkaTemplate;

    public PatientService(PatientRepository patientRepository){
        this.patientRepository=patientRepository;
    }
    @GetMapping("/patientdata/{id}")
    public List<DataEntry> GetPatientData(@PathVariable String id){
        return patientRepository.findByPatientId(id);
    }
    @PostMapping("/storedata")
    public long PostData(@RequestBody DataEntry dataEntry){
        Date now = new Date();
        dataEntry.setTime(now);
        try {
            kafkaTemplate.send("patient-data", dataEntry).get(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        } catch (TimeoutException e) {
            throw new RuntimeException(e);
        }
        return now.getTime();
    }


}
