package fi.antti.sensitivewriter.configuration;

import fi.antti.sensitivewriter.domain.DataEntry;
import org.apache.kafka.common.serialization.Serdes;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;

public class DataEntrySerde extends Serdes.WrapperSerde<DataEntry> {
    public DataEntrySerde(){
        super(new JsonSerializer<>(), new JsonDeserializer<>(DataEntry.class));
    }
}
