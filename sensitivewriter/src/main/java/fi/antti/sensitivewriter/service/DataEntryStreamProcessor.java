package fi.antti.sensitivewriter.service;


import fi.antti.sensitivewriter.domain.DataEntry;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Predicate;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Value;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class DataEntryStreamProcessor {

    MainRepository mainRepository;

    @Value("${spring.kafka.topic.allert}")
    private String alerttopic;
    private Map<String, List<String>> illegalRead = new HashMap<>();
    public  DataEntryStreamProcessor (MainRepository repository){
        this.mainRepository=repository;
        illegalReads();
    }
    public void process(KStream<String, DataEntry> stream) {
        stream.filter(new Predicate<String, DataEntry>() {
            @Override
            public boolean test(String s, DataEntry dataEntry) {
                return illegalRead.get(dataEntry.getPatientId()).contains(dataEntry.getIdOfUser());
            }
        }).to(alerttopic);
    }

    private void illegalReads(){
        //in real life this would be fetched for other system and if necessary cached locally
        this.illegalRead.put("patient",List.of("spouse","ex"));
    }

}
