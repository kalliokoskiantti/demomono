package fi.antti.sensitivewriter.domain;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Document
public class LogEntry {

    private String userId;
    private String operation;
    private Date time;
    private String patientId;

}
