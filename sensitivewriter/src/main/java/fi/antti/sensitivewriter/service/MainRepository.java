package fi.antti.sensitivewriter.service;

import fi.antti.sensitivewriter.domain.DataEntry;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MainRepository extends CrudRepository<DataEntry,String> {
}
