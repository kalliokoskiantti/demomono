package fi.antti.restservice.service;

import fi.antti.restservice.domain.DataEntry;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PatientRepository extends CrudRepository<DataEntry,String> {

    List<DataEntry> findByPatientId(String patientId);

}
