package fi.antti.restservice.domain;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Document
public class DataEntry {
    private String id;
    private String name;
    private String patientId;
    private String text;
    private Date time;
    private String idOfUser;
}
